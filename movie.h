#ifndef MOVIE_H
#define MOVIE_H

/// \file

#include <string>
#include <QDebug>
#include "filemanip.h"
#include <QString>
#include <QStringList>
#include <iostream>

using namespace std ;

/// \fn string getMovieByName(string moviename, filemanip &file)
/// \~English
/// \brief Finds the information of a movie given its name.
/// \param moviename The name of the movie
/// \param file A file pointer
/// \return One line with the information of the movie separated by | (pipes)
/// \~Spanish
/// \brief Encuentra la informacion de una pelicula dado su nombre.
/// \param moviename Nombre de la pelicula
/// \param file Un apuntador a archivo.
/// \return Una linea con la informacion de la pelicula separada por |.
string getMovieByName(string moviename, filemanip &file ) ;

/// \fn string getMovieByPosition(int position, filemanip &file)
/// \~English
/// \brief Finds a movie in position position of a file and returns the
/// name of the movie
/// \param position Position of the movie in the file
/// \param file A file pointer
/// \return The name of the movie
/// \~Spanish
/// \brief Encuentra una pelicula en posicion position de un archivo y devuelve
/// el nombre de la pelicula.
/// \param position Posicion de la pelicula en el archivo
/// \param file Un apuntador a archivo.
/// \return El nombre de la pelicula
string getMovieByPosition(int position, filemanip &file) ;

/// \fn void showMovies(filemanip &file, int start, int end)
/// \~English
/// \brief Display the information of the movies in a file from line start to line
/// end.
/// \param file Pointer to a file.
/// \param start Line in the file to start showing movie info.
/// \param end Line in the file to stop showing movie information.
/// \~Spanish
/// \brief Despliega la informacion de las peliculas en un archivo desde la linea start
/// hasta la line end.
/// \param file Apuntador a un archivo.
/// \param start Linea en el archivo desde donde desplegar info de las peliculas
/// \param end Linea en el archivo hasta donde deplegar info de las peliculas
void showMovies(filemanip &file, int start=1, int end=10) ;

/// \fn void showMovies(filemanip &file, string keyword)
/// \~English
/// \brief Display the information of the movies whose name contains a given keyword
/// \param file Pointer to a file.
/// \param keyword Keyword to search in the name of the movies.
/// \~Spanish
/// \brief Despliega la informacion de las peliculas cuyo nombre contenga una palabra
/// clave dada.
/// \param file Apuntador a un archivo.
/// \param keyword Palabra clave para buscar en el nombre de las peliculas.
void showMovies(filemanip &file, string keyword);


/// \fn void showMovie(string movieinfo)
/// \~English
/// \brief Display the movie information of a | separated line with movie info.
/// Example line: Ninja Turtles: The Next Mutation, Vol. 1|Shout! Factory||Out|2.0|4:3|19.93|NR|1997|
/// \param movieinfo The information of the movie in one line separated by |
/// \~Spanish
/// \brief Despliega la informacion de una pelicula de una linea con la informacion de la
/// pelicula separada por |.
/// Ejemplo de la linea: Ninja Turtles: The Next Mutation, Vol. 1|Shout! Factory||Out|2.0|4:3|19.93|NR|1997|
/// \param movieinfo La informacion de la pelicula en una linea separada por |
void showMovie(string movieinfo) ;



/// \fn string getMovieName(string movieinfo)
/// \~English
/// \brief Given a movie line from the file returns the movie name.
/// \param movieinfo The information of the movie in one line separated by |
/// \return Movie name
/// \~Spanish
/// \brief Dado una linea de una pelicula del archivo devuelve el nombre de la pelicula
/// \param movieinfo La informacion de la pelicula en una linea separada por |
/// \return El nombre de la pelicula
string getMovieName(string movieinfo) ;

/// \fn string getMovieRating(string movieinfo)
/// \~English
/// \brief Given a movie line from the file returns the movie rating.
/// \param movieinfo The information of the movie in one line separated by |
/// \return The movie rating
/// \~Spanish
/// \brief Dado una linea de una pelicula del archivo devuelve la clasificacion
/// de una pelicula
/// \param movieinfo La informacion de la pelicula en una linea separada por |
/// \return La clasificacion de la pelicula
string getMovieRating(string movieinfo) ;

/// \fn string getMovieYear(string movieinfo)
/// \~English
/// \brief Given a movie line from the file returns the movie release year.
/// \param movieinfo The information of the movie in one line separated by |
/// \return The movie release year.
/// \~Spanish
/// \brief Dado una linea de una pelicula del archivo devuelve el ano de lanzamiento
/// de la pelicula
/// \param movieinfo La informacion de la pelicula en una linea separada por |
/// \return El ano de lanzamiento de la pelicula
string getMovieYear(string movieinfo) ;

/// \fn string getMovieGenre(string movieinfo)
/// \~English
/// \brief Given a movie line from the file returns the movie genre.
/// \param movieinfo The information of the movie in one line separated by |
/// \return The movie genre.
/// \~Spanish
/// \brief Dado una linea de una pelicula del archivo devuelve el genero
/// de la pelicula
/// \param movieinfo La informacion de la pelicula en una linea separada por |
/// \return El genero de la pelicula
string getMovieGenre(string movieinfo) ;

/// \fn void getMovieInfo(string, string &, string &, string &, string &)
/// \~English
/// \brief Given a movie line from the file returns by reference the movie name,
/// rating, year, and genre.
/// \param movieinfo The information of the movie in one line separated by |
/// \~Spanish
/// \param name Reference variable to return the movie name
/// \param rating Reference variable to return the movie rating
/// \param year Reference variable to return the movie release year
/// \param genre Reference variable to return the movie genre
/// \brief Dado una linea de una pelicula del archivo devuelve por referencia
/// el nombre, clasificion, ano de lanzamiento y genero de una pelicula.
/// \param name Variable de referencia para devolver el nombre de la pelicula
/// \param rating Variable de referencia para devolver la clasificacion de la pelicula
/// \param year Variable de referencia para devolver el ano de lanzamiento de la pelicula
/// \param genre Variable de referencia para devolver el genero de la pelicula
void getMovieInfo(string movieinfo, string &rating, string &year,
                  string &genre);



#endif // MOVIE_H

